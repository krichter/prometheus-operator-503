#!/usr/bin/python3

import subprocess as sp
import logging


def delete_disks(gcloud="gcloud"):
    disks_output = sp.check_output([gcloud, "compute", "disks", "list"]).decode("utf-8").strip()
    logging.debug("disks_output: %s" % (disks_output,))
    if disks_output == "":
        return
    disks_output_lines = disks_output.split("\n")
    for line in disks_output_lines:
        line_split = line.split()
        disk_name = line_split[0]
        if disk_name == "NAME":
            continue
        if not disk_name.startswith("gke-prometheus"):
            continue
        sp.check_call([gcloud, "compute", "disks", "delete", "--quiet", disk_name])


if __name__ == "__main__":
    logging.basicConfig(level=logging.INFO)
    delete_disks()
