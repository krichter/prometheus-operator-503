#!/usr/bin/python3

import subprocess as sp
import time
import logging


def wait_for_ingress_ip(installation_namespace="default", service_name="echo-service", output_file="frontend_ingress_ip", kubectl="kubectl"):
    wait_max_secs = 600
    wait_interval = 5
    count = 0
    ip = ""
    while count < wait_max_secs:
        ip = sp.check_output([kubectl, "get", "service",
                              "--namespace=%s" % (installation_namespace,),
                              "--output=jsonpath='{.items[?(@.metadata.name==\"%s\")].status.loadBalancer.ingress[0].ip}'" % (service_name,)]) \
                .decode("utf-8").strip().strip("'")
        logging.debug("ip: %s" % (ip,))
        if ip != "":
            break
        count += wait_interval
        time.sleep(wait_interval)
    if ip == "":
        raise ValueError("ip could not be retrieved within %d seconds" % (wait_max_secs,))
    if output_file is None:
        print(ip)
    else:
        with open(output_file, "w") as file:
            file.write(ip)


if __name__ == "__main__":
    logging.basicConfig(level=logging.DEBUG)
    wait_for_ingress_ip()

